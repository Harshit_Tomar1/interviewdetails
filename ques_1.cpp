//HERE IS THE CODE FOR MY FIRST CODING QUESTION ASKED IN THE INTERVIEW
// In a given Array of strings representing the votes in an election print the winner.
// work on corner case (one who got the last top no. of votes is the winner in case there is a tie).

int32_t main() {
int n;
cin>>n;
map<string,int>mp;
string winner;
int ans{INT_MIN};
for(int i{0};i<n;i++){
    string a;
    cin>>a;
    mp[a]++;
    if(mp[a]>=ans){
        winner=a;
        ans=mp[a];
    }
}
cout<<winner<<endl;
return 0;
}